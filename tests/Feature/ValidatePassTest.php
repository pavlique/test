<?php

namespace Tests\Feature;

use App\ValidatePass;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ValidatePassTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPassLength()
    {
        $this->assertTrue(ValidatePass::validateLength("123456"));
    }
}
