<?php
/**
 * Created by PhpStorm.
 * User: jane
 * Date: 24.10.18
 * Time: 21:50
 */

namespace App;


class ValidatePass
{

    const  MIN_LN = 6;
    const MAX_LN = 12;

    public static function validateLength($str){
        $ln = strlen($str);
        if ($ln >= static::MIN_LN && $ln <= static::MAX_LN) return true;
        return false;

    }

}
